#!/usr/bin/env python3

import os

print("# Default compose configuration")
with open(os.environ["HOME"] + "/.cm/MainCompose", "r") as main_compose:
    for line in main_compose:
        print(line.strip())
print("# Custom compose configuration")


# from `man keysyms`
keysyms = {
    " ": "space",
    "!": "exclam",
    "\"": "quotedbl",
    "#": "numbersign",
    "$": "dollar",
    "%": "percent",
    "&": "ampersand",
    "'": "apostrophe",
    "(": "parenleft",
    ")": "parenright",
    "*": "asterisk",
    "+": "plus",
    ",": "comma",
    "-": "minus",
    ".": "period",
    "/": "slash",
    ":": "colon",
    ";": "semicolon",
    "<": "less",
    "=": "equal",
    ">": "greater",
    "?": "question",
    "@": "at",
    "[": "bracketleft",
    "\\": "backslash",
    "]": "bracketright",
    "^": "asciicircum",
    "_": "underscore",
    "`": "grave",
    "{": "braceleft",
    "|": "bar",
    "}": "braceright",
    "~": "asciitilde",
}

defs = {}

def map_alphabets(defs, target, original, key_seq_mapping):
    for target_letter, original_letter in zip(target, original):
        key_sequence = key_seq_mapping(original_letter)
        defs[key_sequence] = target_letter

greek = "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ"
latin_greek_map = "ABGDEZHQIKLMNCOPRSTUFXYW" # unused: JV
map_alphabets(defs, greek, latin_greek_map, lambda l: ("g", l))
map_alphabets(defs, greek.lower(), latin_greek_map.lower(), lambda l: ("g", l))

cyrillic =           "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
latin_cyrillic_map = "ABVGDEXZIJKLMNOPRSTUFH-C----_--Y" # QW
map_alphabets(defs, cyrillic, latin_cyrillic_map, lambda l: ("r", l))
map_alphabets(defs, cyrillic.lower(), latin_cyrillic_map.lower(), lambda l: ("r", l))

blackboard = "𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ"
latin_blackboard_map = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
map_alphabets(defs, blackboard, latin_blackboard_map, lambda l: (l, l))

superscript = "⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻⁼⁽⁾ⁿⁱ"
superscript_map = "0123456789+-=()ni"
map_alphabets(defs, superscript, superscript_map, lambda l: ("^", l))

subscript = "₀₁₂₃₄₅₆₇₈₉₊₋₌₍₎ₐₑₕᵢⱼₖₗₘₙₒₚᵣₛₜᵤᵥₓ"
subscript_map = "0123456789+-=()aehijklmnoprstuvx"
map_alphabets(defs, subscript, subscript_map, lambda l: ("_", l))

defs["/\\"] = "∧"
defs["^^"] = "∧"
defs["\\/"] = "∨"
defs["^_"] = "⊻"
defs["vv"] = "∨"
defs["fa"] = "∀"
defs["ex"] = "∃"
defs["nx"] = "∄"
defs["el"] = "∈"
defs["un"] = "∩"
defs["in"] = "∪"
defs["tf"] = "∴"
defs["cr"] = "×"
defs["su"] = "⊆"
defs["sU"] = "⊂"
defs["/su"] = "⊈"
defs["/ss"] = "⊄"
defs["0/"] = "∅"
defs["fn"] = "↦"
defs["]>"] = "⟩"
defs["[<"] = "⟨"
defs["on"] = "⍝"
defs["|-"] = "⊢"
defs["-|"] = "⊣"
defs["_|"] = "⊥"
defs["|_"] = "⊥"
defs["__"] = "¯"
defs["=="] = "≡"
defs["[f"] = "⌊"
defs["]f"] = "⌋"
defs["[c"] = "⌈"
defs["]c"] = "⌉"
defs["*O"] = "⍟"
defs["o."] = "∘"
defs["tt"] = "⊤"
defs["ii"] = "⍳"
defs["i_"] = "⍸"
defs["::"] = "¨"
defs["~:"] = "⍨"
defs[":~"] = "⍨"
defs[":*"] = "⍣"
defs["*:"] = "⍣"
defs["[]"] = "⎕"
defs["o_"] = "⍎"
defs["_o"] = "⍎"
defs["[|"] = "⌷"
defs["|]"] = "⌷"
defs["o*"] = "⍟"
defs["*o"] = "⍟"
defs["``"] = "\u0300"
defs["`'"] = "\u0301"
defs["`^"] = "\u0302"
defs["`~"] = "\u0303"
defs["`-"] = "\u0304"
defs["`@"] = "\u0306"
defs["`."] = "\u0307"
defs["`:"] = "\u0308"
defs["`*"] = "\u030A"
defs["jj"] = "ʒ"
defs["sh"] = "ʃ"
defs["dg"] = "°"
defs["gj"] = "ς"
defs["zj"] = "\u200D"

for keys, char in defs.items():
    letter_to_symbol = lambda letter: f"<{keysyms[letter]}>" if letter in keysyms else f"<{letter}>"

    key_sequence = " ".join(map(letter_to_symbol, keys))
    print(f'<Multi_key> {key_sequence} : "{char}"')

# vim:ft=py
