#!/bin/zsh
message_text=$(dunstctl history | jq -r '.data|.[0]|.[]|.summary.data' | dmenu -l 20 -fn "FiraCode")
message_id=$(dunstctl history | jq --arg text "$message_text" '.data|.[0]|.[]| select(.summary.data == $text) | .id.data')
dunstctl history-pop "$message_id"z
