
local getline = vim.fn['getline']
local foldlevel = vim.fn['foldlevel']

function is_line_empty(line)
    return line:match("^%s+\n%") ~= nil
end

function get_fold_level(linenum)
    local line = getline(linenum)
    
    if(is_line_empty(line))
    then
        -- Current line is empty
        -- check to see if next line is empty too
        local current_level = foldlevel(linenum - 1)
        if(is_line_empty(getline(linenum - 1)))
        then
            current_level = current_level - 1
        end

        if(is_line_empty(getline(linenum + 1)))
        then
            -- Next line is also empty
            -- End the section
            return "<"..tostring(current_level)
            --return tostring(current_level)
        else
            -- Next line has content
            return tostring(current_level)
        end
    else 
        local header_match = line:match("^%*+")
        if(header_match ~= nil)
        then
            -- The current line is a header
            local header_level = header_match:len()
            return ">"..tostring(header_level)
            --return tostring(header_level)
        else
            -- The current line is not a header
            return tostring(foldlevel(linenum - 1))
        end
    end

    return "-1" 
end

function setup()
    vim.api.nvim_create_autocmd({"FileType"}, {
        pattern = {"norg"},
        callback = function()
            vim.opt_local.foldmethod = "expr"
            --vim.opt_local.foldexpr = "v:lua.require'neorg_folding'.get_fold_level(v:lnum)"
            vim.opt_local.foldexpr = "nvim_treesitter#foldexpr()"
        end
    })
end

return {
    get_fold_level = get_fold_level,
    setup = setup,
}
