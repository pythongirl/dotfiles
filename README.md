# Dotfiles
A repository for managing my dotfile setup. Uses the [Chezmoi](https://www.chezmoi.io/)
configuration management tool.
