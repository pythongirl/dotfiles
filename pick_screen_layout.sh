#!/usr/bin/zsh
layout_dir=~/.screenlayout
selected_layout=$(ls ${layout_dir} | dmenu -fn "FiraCode")
if [ $? -eq 0 ]; then
    exec "${layout_dir}/${selected_layout}"
fi
